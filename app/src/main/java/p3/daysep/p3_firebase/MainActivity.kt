package p3.daysep.p3_firebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QueryDocumentSnapshot
import kotlinx.android.synthetic.main.activity_main.*
import java.util.ArrayList

class MainActivity : AppCompatActivity(), View.OnClickListener{
    val COLLECTION = "students"
    val F_ID = "id"
    val F_NAME ="name"
    val F_ADDRESS = "address"
    val F_PHONE = "phone"
    var docID = ""
    lateinit var db : FirebaseFirestore
    lateinit var alStudent  : ArrayList<HashMap<String, Any>>
    lateinit var adapter: SimpleAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        alStudent = ArrayList()
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)
    }
    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener {querySnapshot, e ->
            if (e != null) Log.d("fireStore",e.localizedMessage)
            showData()
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm :HashMap<String, Any> = alStudent.get(position)
        docID = hm.get(F_ID).toString()
        edId.setText(docID)
        edName.setText(hm.get(F_NAME).toString())
        edAddress.setText(hm.get(F_ADDRESS).toString())
        edPhone.setText(hm.get(F_PHONE).toString())
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert  ->{
                val hm = HashMap<String, Any>()
                hm.set(F_ID, edId.text.toString())
                hm.set(F_NAME, edName.text.toString())
                hm.set(F_ADDRESS, edAddress.text.toString())
                hm.set(F_PHONE, edPhone.text.toString())
                db.collection(COLLECTION).document(edId.text.toString()).set(hm).addOnSuccessListener {
                    Toast.makeText(this, "Data Successfully Added", Toast.LENGTH_SHORT).show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data Unsuccessfully Added : ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btnUpdate  ->{
                val hm = HashMap<String, Any>()
                hm.set(F_ID, docID)
                hm.set(F_NAME, edName.text.toString())
                hm.set(F_ADDRESS, edAddress.text.toString())
                hm.set(F_PHONE, edPhone.text.toString())
                db.collection(COLLECTION).document(docID).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this, "Data Successfully Updated", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener { e ->
                    Toast.makeText(this, "Data Unsuccessfully Updated : ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btnDelete  ->{
                db.collection(COLLECTION).whereEqualTo(F_ID, docID).get()
                .addOnSuccessListener {
                    result ->
                    for (doc : QueryDocumentSnapshot in result){
                        db.collection(COLLECTION).document(doc.id).delete()
                                .addOnSuccessListener {
                                    Toast.makeText(this, "Data Successfully Delete", Toast.LENGTH_SHORT).show()
                                }
                                .addOnFailureListener { e ->
                                    Toast.makeText(this, "Data Unsuccessfully Delete : ${e.message}",
                                            Toast.LENGTH_SHORT).show()
                                }
                    }
                }.addOnFailureListener{e ->
                        Toast.makeText(this, "Can't get data references ${e.message}",
                            Toast.LENGTH_SHORT).show()
                        }
            }
        }
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alStudent.clear()
            for (doc: QueryDocumentSnapshot in result){
                val hm = HashMap<String, Any>()
                hm.set(F_ID, doc.get(F_ID).toString())
                hm.set(F_NAME, doc.get(F_NAME).toString())
                hm.set(F_ADDRESS, doc.get(F_ADDRESS).toString())
                hm.set(F_PHONE, doc.get(F_PHONE).toString())
                alStudent.add(hm)
            }
            adapter = SimpleAdapter(this,alStudent,R.layout.row_data,
                arrayOf(F_ID,F_NAME,F_ADDRESS,F_PHONE),
                intArrayOf(R.id.tdId, R.id.txName, R.id.txAddress, R.id.txPhone))
            lsData.adapter = adapter
        }
    }
}
